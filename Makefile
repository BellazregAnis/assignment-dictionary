ASM=nasm
ASM_FLAGS= -f elf64
LOAD=ld


lib.o: lib.asm 
	$(ASM) $(ASM_FLAGS) lib.asm -o lib.o

dict.o: dict.asm lib.inc
	$(ASM) $(ASM_FLAGS) dict.asm -o dict.o

main.o: main.asm lib.inc colon.inc words.inc
	$(ASM) $(ASM_FLAGS) main.asm -o main.o

main: main.o dict.o lib.o
	$(LOAD) main.o dict.o lib.o -o main







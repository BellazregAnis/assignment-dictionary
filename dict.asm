%include "lib.inc"

global find_word


section .text

;rdi - pointer to the null-term string
;rsi- pointer to the beginning of the dictionary
;If a suitable occurrence is found, it will return the address of the beginning of the occurrence in the dictionary (not the values), otherwise it will return 0.

find_word:
	push rsi
	add rsi, 8 
	call string_equals  
	pop rsi
	cmp rax,1
	je .success
		
 	mov rsi, [rsi] 
 	cmp rsi, 0
 	je .end
 				                          
	jmp find_word	
	
.success:
	mov rax, rsi 
	ret
.end:
	xor rax, rax
	ret



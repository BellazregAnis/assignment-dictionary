section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    mov rax, 60
    syscall
      

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0          
    je .end
    inc rax                        
    jmp .loop
.end:
    ret  


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rsi, rdi				
	call string_length 
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rdi, rsp
	call print_string
	pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:    
    push rbx
    push rdx 
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rbx, 10
    dec rsp
    mov byte [rsp], 0
    .div_lp:
        xor rdx, rdx
        div rbx
        add rdx, 48
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .write_n
        jmp .div_lp
    .write_n:
        mov rdi, rsp
        call print_string
        mov rsp, rbp
        pop rbp
        pop rdx
        pop rbx
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint                              
    push rdi
    mov rdi, '-'   
    call print_char                 
    pop rdi
    neg rdi 
    jmp print_uint



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .no
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
.no:
    xor rax, rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push rax
    mov rsi, rsp
    xor rdx, rdx
    inc rdx
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13 
    push r12
    xor r12, r12 
    xor r13, r13
    mov r13, rdi
.space:
    call read_char 
    cmp rax, 0     
    je .success      
    cmp rax, 0x9
    je .space
    cmp rax, 0xA
    je .success
.word:
    cmp rsi, r12
    je .overbuf
    mov byte[r13 + r12], al
    inc r12
    call read_char
    cmp rax, 0
    je .success
    cmp rax, 0x9
    je .success
    cmp rax, 0xA
    je .success
    jmp .word
.success:
    xor rax, rax
    mov byte[r13 + r12], al
    mov rax, r13
    mov rdx, r12
    jmp .end
.overbuf:
    xor rax, rax
.end:
    pop r12 
    pop r13
    ret

   

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor r9, r9
    mov r8, 10
.loop:
    mov sil, byte[rdi+r9]
    test sil, sil
    jz .end
    cmp sil, 48
    jb .end
    cmp sil, 57
    ja .end
    sub sil, 48
    inc r9
    mul r8
    add rax, rsi
    jmp .loop
.end:
    mov rdx, r9
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rsi, rsi
    mov sil, byte[rdi]
    cmp sil, '-'
    je .signed
    call parse_uint
    ret
.signed:
    inc rdi
    call parse_uint
    test rdx, rdx
    jnz .end
    ret 
.end:
    inc rdx
    neg rax
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    	call string_length
	cmp rax, rdx
	jae .error
.loop:
    mov dl, byte[rdi]
    mov byte[rsi], dl
    inc rdi
    inc rsi
    cmp dl, 0
    jne .loop
    ret
.error:
    xor rax, rax
    ret

    xor rax, rax
    ret

; Outputs an error to stderr
print_error:
    call string_length
    mov     rdx, rax
    mov     rsi, rdi
    mov     rax, 1
    mov     rdi, 2 
    syscall
    ret



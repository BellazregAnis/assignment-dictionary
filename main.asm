
global _start
extern find_word



section .rodata
msgerror1: db "Invalid input", 0
msgerror2: db "Key not found", 0
%include 'colon.inc'
%include "words.inc"
%include "lib.inc"


section .bss
buffer: resb 256

section .data

section .text


_start:

mov rdi, buffer 
mov rsi, 256	


call read_word
cmp rax, 0 
je .readerror


mov rdi, rax	
mov rsi, dict_start 
call find_word 

cmp rax, 0
je .notfounderror


add rax, 8 
mov rdi, rax
call string_length
add rdi,rax 
add rdi, 1 
 
call print_string
jmp  .end


.readerror:
	mov rdi, msgerror1
	call print_error
	call exit
.notfounderror:
	mov rdi, msgerror2
	call print_error
	call exit
.end:
	call exit


